const gulp = require('gulp');
const babel = require('gulp-babel');

gulp.task('default', () =>
  gulp.src('./src/**/*.js')
    .pipe(babel())
    .on('error', onError)
    .pipe(gulp.dest('build'))
);

gulp.task('copy', () =>
  gulp.src('src/**/*.json')
    .pipe(gulp.dest('build'))
);

gulp.task('watch', function () {
  gulp.watch('./src/**/*.js', [ 'default', 'copy' ]);
});

function onError (err) {
  console.log(err);
  this.emit('end');
}