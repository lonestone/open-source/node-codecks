import normalizeDescription from '../lib/node-mate/normalize-description';
import createDenormalizeResultsFn from '../lib/node-mate/denormalize-results';
import requireDirectory from 'require-directory';

test('It denormalises correctly', () => {


  const fileNameToModelDescription = requireDirectory(module, '../shared_models');
  const descriptions = Object.keys(fileNameToModelDescription)
    .map(fileName => fileNameToModelDescription[fileName])
    .reduce((m, d) => {
      m[ d.name ] = normalizeDescription({ ...d });
      return m;
    }, {});
  const denormalizeResults = createDenormalizeResultsFn(descriptions);

  const input = {
    "_root": {loggedInUser: 2},
    user: {2: {
      id: 2,
      name: "Daniel"
    }}
  }

  expect(denormalizeResults(input)).toMatchSnapshot()
})