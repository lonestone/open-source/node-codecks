

export default function createFn(descriptions) {

  const processInstance = (instance, modelName, result) => {
    const findInstance = (modelName, id) => {
      if (!id) return null;
      return result[modelName][id];
    }
    const modelDesc = descriptions[modelName];
    Object.keys(instance).forEach(field => {
      const strippedField = field.replace(/\(.*\)/g, "").replace(/(last|first)(N\d+)?:/g, "");
      const hasManyDesc = modelDesc.belongsTo[strippedField] || modelDesc.hasMany[strippedField];
      if (hasManyDesc) {
        instance[field] = Array.isArray(instance[field])
          ? instance[field].map(id => findInstance(hasManyDesc.model, id))
          : findInstance(hasManyDesc.model, instance[field])
      }
    })
  }

  /*
  input looks like `
  {
    _root: {loggedInUser: 2},
    user: {2: {id: 2, name: Daniel}}
  }
  `

  output looks like `
  {
    _root: {
      loggedInUser: {id: 2, name: Daniel},
    }
    user: {2: {id: 2, name: Daniel}}
  }
  `
  */
  return function denormalizeResults(result) {
    Object.keys(result).forEach(modelName => {
      if (modelName === "_root") {
        processInstance(result[modelName], modelName, result);
      } else {
        const modelInstances = result[modelName];
        Object.keys(modelInstances).forEach(id => {
          processInstance(modelInstances[id], modelName, result);
        });
      }
    })
    return result;
  }
}