const today = new Date();

const defaults = {
  array: [],
  json: {},
  date: today,
  int: 0,
  day: { year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate() }
};

function fieldDescFromDesc (desc = {}) {
  return {
    ...{
      type: "default",
      defaultValue: defaults[ desc.type ] !== undefined ? defaults[ desc.type ] : "unknown",
      deps: []
    }, ...desc
  };
}

export default function normalizeDescription (desc) {
  if (desc.name === "_root") {
    desc.idPropAsArray = [];
  } else {
    desc.idProp = desc.idProp || "id";
    desc.idPropAsArray = Array.isArray(desc.idProp) ? desc.idProp : [ desc.idProp ];
  }

  desc.fields = [ ...desc.fields, ...desc.idPropAsArray ].reduce(
    (fields, field) => {
      if (typeof field === "string") {
        fields[ field ] = fieldDescFromDesc();
      } else {
        Object.keys(field).forEach(fieldLabel => {
          const fieldDesc = field[ fieldLabel ];
          fields[ fieldLabel ] = fieldDescFromDesc(typeof fieldDesc === "string" ? {} : fieldDesc);
        });
      }
      return fields;
    },
    {}
  );

  function hasManyDescFromString (key) {
    return { model: key.replace(/s$/, ""), isSingleton: false, fk: `${desc.name}Id`, deps: [] };
  }

  function belongsToDescFromString (key) {
    return { model: key, fk: `${key}Id`, deps: [] };
  }

  desc.hasMany = (desc.hasMany || []).reduce(
    (m, entry) => {
      if (typeof entry === "string") {
        m[ entry ] = hasManyDescFromString(entry);
      } else {
        Object.keys(entry).forEach(relName => {m[ relName ] = { ...hasManyDescFromString(relName), ...entry[ relName ] };});
      }
      return m;
    },
    {}
  );

  desc.belongsTo = (desc.belongsTo || []).reduce(
    (m, entry) => {
      if (typeof entry === "string") {
        m[ entry ] = belongsToDescFromString(entry);
      } else {
        Object.keys(entry).forEach(relName => {m[ relName ] = { ...belongsToDescFromString(relName), ...entry[ relName ] };});
      }
      return m;
    },
    {}
  );

  return desc;
}