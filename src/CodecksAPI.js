import normalizeDescription from './lib/node-mate/normalize-description';
import createDenormalizeResultsFn from './lib/node-mate/denormalize-results';
import requireDirectory from 'require-directory';
import request from 'request-promise';

const fileNameToModelDescription = requireDirectory(module, "./shared_models");

export default class CodecksAPI {
  constructor (subdomain, token, host = "https://api.codecks.io") {
    this.subdomain = subdomain;
    this.token = token;
    this.host = host;

    this.sessionId = 'node-codecks';

    this.serverRequest = this.serverRequest.bind(this);
    this.fetch = this.fetch.bind(this);
    this.dispatcher = this.dispatcher.bind(this);

    const descriptions = Object.keys(fileNameToModelDescription)
      .map(fileName => fileNameToModelDescription[fileName])
      .reduce((m, d) => {
        m[ d.name ] = normalizeDescription({ ...d });
        return m;
      }, {});
    this.denormalizeResults = createDenormalizeResultsFn(descriptions);
  }

  async serverRequest (method, path, data) {
    let options = {
      method: method,
      uri: this.host + path,
      body: data,
      json: true,
      headers: {
        'X-Auth-Token': this.token,
        'X-Account': this.subdomain,
        'Accept': 'application/vnd.codecks-v2'
      }
    };

    return request(options);
  }

  async fetch (query) {
    return this.serverRequest("post", "", { query: query })
      .then(this.denormalizeResults);
  }

  async dispatcher (actionName, data) {
    return this.serverRequest("post", `/dispatch/${actionName}`, data)
      .then(res => res.payload);
  }
}